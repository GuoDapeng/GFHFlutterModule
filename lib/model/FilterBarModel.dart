import 'package:flutter/material.dart';

class FilterBarModel extends ChangeNotifier {
  int _filterIndex = -1;

  int get index => _filterIndex;

  void setIndex(int index) {
    if (_filterIndex == index) {
      _filterIndex = -1;
    } else {
      _filterIndex = index;
    }
    notifyListeners();
  }
}
