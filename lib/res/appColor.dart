import 'package:flutter/material.dart';

class AppColor {
  static const Color red = Color(0xFFC50119);
  static const Color gray = Color(0xFF181818);
  static const Color white = Color(0xFFFFFFFF);
  static const Color bColor = Color(0xFFF1F1F1);
  static const Color bColor_90 = Color(0x90F1F1F1);
  static const Color textBlack = Color(0XFF0C0C0C);
  static const Color textGray = Color(0xFF999999);
  static const Color textGray_6 = Color(0xFF666666);
}
