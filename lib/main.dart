import 'package:flutter/material.dart';

import 'res/appColor.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'ui/page/home.dart';
import 'ui/page/news.dart';
import 'ui/page/housing.dart';
import 'ui/page/school.dart';
import 'ui/page/me.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _selectedIndex = 0;
  static List<Widget> _widgetOptions = <Widget>[
    Home(),
    News(),
    Housing(),
    School(),
    Me()
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 750, height: 1334);
    return Scaffold(
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Image.asset(
              _selectedIndex == 0
                  ? 'assets/images/home_b.png'
                  : 'assets/images/home_d.png',
              width: ScreenUtil().setWidth(50),
              gaplessPlayback: true,
            ),
            title: Text('首页'),
          ),
          BottomNavigationBarItem(
            icon: Image.asset(
              _selectedIndex == 1
                  ? 'assets/images/news_b.png'
                  : 'assets/images/news_d.png',
              width: ScreenUtil().setWidth(50),
              gaplessPlayback: true,
            ),
            title: Text('资讯'),
          ),
          BottomNavigationBarItem(
            icon: Image.asset(
              _selectedIndex == 2
                  ? 'assets/images/housing_b.png'
                  : 'assets/images/housing_d.png',
              width: ScreenUtil().setWidth(50),
              gaplessPlayback: true,
            ),
            title: Text('找房'),
          ),
          BottomNavigationBarItem(
            icon: Image.asset(
              _selectedIndex == 3
                  ? 'assets/images/school_b.png'
                  : 'assets/images/school_d.png',
              width: ScreenUtil().setWidth(50),
              gaplessPlayback: true,
            ),
            title: Text('学区'),
          ),
          BottomNavigationBarItem(
            icon: Image.asset(
              _selectedIndex == 4
                  ? 'assets/images/me_b.png'
                  : 'assets/images/me_d.png',
              width: ScreenUtil().setWidth(50),
              gaplessPlayback: true,
            ),
            title: Text('我的'),
          ),
        ],
        type: BottomNavigationBarType.fixed,
        selectedItemColor: AppColor.red,
        unselectedItemColor: AppColor.gray,
        selectedFontSize: ScreenUtil().setWidth(22),
        unselectedFontSize: ScreenUtil().setWidth(22),
        showUnselectedLabels: true,
        currentIndex: _selectedIndex,
        onTap: _onItemTapped,
      ),
    );
  }
}
