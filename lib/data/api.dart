import 'package:dio/dio.dart';

class Api {
  static const String _root_path = 'http://dev.gfb.qinghuiche.com';

  static const String apiIndexGetProjectsList = '/api/index/getProjectsList';

  // 拼接url
  static String getPath({String path: '', int page, int limit}) {
    StringBuffer sb = new StringBuffer(_root_path);
    sb.write(path);
    if (page != null) {
      sb.write('/page/$page');
    }
    if (limit != null) {
      sb.write('/limit/$limit');
    }
    return sb.toString();
  }

  static Future<Response> getProjectsList({int page}) async {
    String url = getPath(
      path: Api.apiIndexGetProjectsList,
      page: page,
      // limit: 1,
    );
    print('page: ' + page.toString());
    return await Dio().get(url);
  }
}
