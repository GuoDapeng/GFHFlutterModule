import 'package:flutter/material.dart';
import '../../res/appStyle.dart';

class Housing extends StatefulWidget {
  @override
  _HousingState createState() => _HousingState();
}

class _HousingState extends State<Housing> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text(
        'Index 2: Housing',
        style: AppStyle.optionStyle,
      ),
    );
  }
}
