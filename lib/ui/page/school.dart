import 'package:flutter/material.dart';
import '../../res/appStyle.dart';

class School extends StatefulWidget {
  @override
  _SchoolState createState() => _SchoolState();
}

class _SchoolState extends State<School> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text(
        'Index 3: School',
        style: AppStyle.optionStyle,
      ),
    );
  }
}
