import 'package:flutter/material.dart';
import '../../res/appStyle.dart';

class News extends StatefulWidget {
  @override
  _NewsState createState() => _NewsState();
}

class _NewsState extends State<News> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text(
        'Index 1: News',
        style: AppStyle.optionStyle,
      ),
    );
  }
}
