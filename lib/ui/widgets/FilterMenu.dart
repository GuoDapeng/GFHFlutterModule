import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../model/FilterBarModel.dart';
import 'RegionFilter.dart';

class FilterMenu extends StatefulWidget {
  const FilterMenu({
    @required this.index,
    Key key,
  }) : super(key: key);

  final int index;

  @override
  _FilterMenuState createState() => _FilterMenuState();
}

class _FilterMenuState extends State<FilterMenu> {
  List<Widget> _filter = [
    RegionFilter(),
  ];

  @override
  Widget build(BuildContext context) {
    int _filterIndex = widget.index;

    var _content;
    if (_filterIndex >= 0 && _filterIndex <= _filter.length - 1) {
      _content = Positioned(
        top: ScreenUtil().setWidth(90),
        child: Column(
          children: <Widget>[
            _filter[_filterIndex],
            GestureDetector(
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                color: Color.fromRGBO(0, 0, 0, 0.3),
              ),
              onTap: () {
                var filterBarData = Provider.of<FilterBarModel>(context);
                filterBarData.setIndex(-1);
              },
              behavior: HitTestBehavior.translucent,
            )
          ],
        ),
      );
    } else {
      _content = Container(height: 0.0, width: 0.0);
    }
    return _content;
  }
}
