import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../res/appColor.dart';

class PropertyListItem extends StatelessWidget {
  const PropertyListItem({
    @required this.data,
    Key key,
  }) : super(key: key);

  final data;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(ScreenUtil().setWidth(28)),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(
              right: ScreenUtil().setWidth(42),
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(
                ScreenUtil().setWidth(8),
              ),
              child: Image.network(
                data['projects_cover_img'],
                width: ScreenUtil().setWidth(214),
                height: ScreenUtil().setWidth(166),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Expanded(
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    data['projects_title'],
                    style: TextStyle(
                      color: AppColor.textBlack,
                      fontSize: ScreenUtil().setWidth(32),
                      fontWeight: FontWeight.bold,
                      height: 1,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      top: ScreenUtil().setWidth(20),
                    ),
                    child: Text(
                      '${data['region_title']} | 建面${data['projects_house_type_area_min']}-${data['projects_house_type_area_max']}m²',
                      style: TextStyle(
                        color: AppColor.textGray_6,
                        fontSize: ScreenUtil().setWidth(26),
                        height: 1,
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      top: ScreenUtil().setWidth(20),
                    ),
                    child: Row(
                      children: <Widget>[
                        Text(
                          data['projects_price_avg'],
                          style: TextStyle(
                            color: AppColor.red,
                            fontSize: ScreenUtil().setWidth(30),
                            fontWeight: FontWeight.bold,
                            height: 1,
                          ),
                        ),
                        Text(
                          '元/m²',
                          style: TextStyle(
                            color: AppColor.red,
                            fontSize: ScreenUtil().setWidth(26),
                            height: 1,
                          ),
                        ),
                      ],
                    ),
                  ),
                  _tag(),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _tag() {
    List<Widget> _tags = [];

    String _projectsIsSell = data['projects_is_sell'].toString();
    if (_projectsIsSell == '1') {
      _tags
        ..add(Container(
          color: AppColor.red,
          margin: EdgeInsets.only(
            right: ScreenUtil().setWidth(10),
          ),
          padding: EdgeInsets.only(
            left: ScreenUtil().setWidth(8),
            right: ScreenUtil().setWidth(8),
          ),
          child: Text(
            '在售',
            style: TextStyle(
              fontSize: ScreenUtil().setWidth(24),
              color: AppColor.white,
              height: 1.26,
            ),
          ),
        ));
    }

    (data['projects_tag_list']).forEach((i) {
      _tags
        ..add(Container(
          color: AppColor.bColor,
          margin: EdgeInsets.only(
            right: ScreenUtil().setWidth(10),
          ),
          padding: EdgeInsets.only(
            left: ScreenUtil().setWidth(8),
            right: ScreenUtil().setWidth(8),
          ),
          child: Text(
            i,
            style: TextStyle(
              fontSize: ScreenUtil().setWidth(24),
              color: AppColor.textGray,
              height: 1.26,
            ),
          ),
        ));
    });

    return Container(
      margin: EdgeInsets.only(
        top: ScreenUtil().setWidth(20),
      ),
      child: Row(
        children: _tags,
      ),
    );
  }
}
